package de.szut.employee_administration_backend.controller.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class QualificationDTO implements Serializable {
    @NotBlank(message = "designation is mandatory")
    private String designation;
}
