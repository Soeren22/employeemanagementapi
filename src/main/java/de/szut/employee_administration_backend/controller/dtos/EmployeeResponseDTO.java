package de.szut.employee_administration_backend.controller.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import de.szut.employee_administration_backend.controller.service.model.QualificationEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@NoArgsConstructor
@Getter
@Setter
public class EmployeeResponseDTO implements Serializable {

    private long id;

    @NotNull(message = "Lastname is mandatory")
    private String lastName;
    @NotNull(message = "Firstname is mandatory")
    private String firstName;
    @NotNull(message = "Street is mandatory")
    private String street;
    @NotBlank(message = "Postcode is mandatory")
    @Size(min = 5, max = 5, message = "Postcode must have 5 characters")
    private String postcode;
    @NotNull(message = "city is mandatory")
    private String city;
    @NotNull(message = "Phonenumber is mandatory")
    private String phone;
}
