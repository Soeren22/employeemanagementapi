package de.szut.employee_administration_backend.controller.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Data
public class EmployeeNameAndSkillDataDTO implements Serializable {
    private Long id;
    private String surname;
    private String firstname;
    private Set<QualificationDTO> skillSet = new HashSet<>();

    public void addQualifcation(QualificationDTO qualification){
        this.skillSet.add(qualification);
    }
}
