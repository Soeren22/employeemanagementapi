package de.szut.employee_administration_backend.controller.service;
import de.szut.employee_administration_backend.controller.exceptionHandling.EmployeeAlreadyExistsException;
import de.szut.employee_administration_backend.controller.exceptionHandling.EmployeeAlreadyHasThisQualificationException;
import de.szut.employee_administration_backend.controller.exceptionHandling.ResourceNotFoundException;
import de.szut.employee_administration_backend.controller.service.model.EmployeeEntity;
import de.szut.employee_administration_backend.controller.service.model.QualificationEntity;
import de.szut.employee_administration_backend.controller.service.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService {
    @Autowired
    private EmployeeRepository repository;

    @Autowired
    private QualificationService qualificationService;

    public EmployeeEntity create(EmployeeEntity employee) {
        if(!this.repository.existsByLastNameAndFirstNameAndStreet(employee.getLastName(), employee.getFirstName(), employee.getStreet())){
            return repository.save(employee);
        }
        else{
            throw new EmployeeAlreadyExistsException("EmployeeEntity already exists");
        }
    }

    public EmployeeEntity addQualification(long e_id, QualificationEntity newQualification) {
        EmployeeEntity employee = this.getEmployeeById(e_id);
        newQualification = this.qualificationService.readByDesignation(newQualification.getDesignation());
        if(employee.hasQualification(newQualification)){
            throw new EmployeeAlreadyHasThisQualificationException("EmployeeEntity already has got this qualification!");
        }
        employee.addQualification(newQualification);
        newQualification.addEmployee(employee);
        return this.repository.save(employee);
    }

    public EmployeeEntity removeQualification(Long id, QualificationEntity qualification){
        EmployeeEntity employee = this.getEmployeeById(id);
        qualification = this.qualificationService.readByDesignation(qualification.getDesignation());
        if(!employee.hasQualification(qualification)){
            throw new ResourceNotFoundException("Employee doesn't have this Qualification!");
        }
        employee.removeQualification(qualification);
        return this.repository.save(employee);
    }

    public List<EmployeeEntity> getAllEmployees() {
        List<EmployeeEntity> listEmployees = this.repository.findAll();
        return listEmployees;
    }

    public EmployeeEntity getEmployeeById(long id) {
        Optional<EmployeeEntity> oemployee = this.repository.findById(id);
        if (oemployee.isEmpty()) {
            return null;
        }
        return oemployee.get();
    }

    public boolean employeeExistsById(long id) {
        return repository.existsById(id);
    }

    public EmployeeEntity updateContactData(EmployeeEntity employee) {
        EmployeeEntity updatedEmployee = getEmployeeById(employee.getId());
        updatedEmployee.setFirstName(employee.getFirstName());
        updatedEmployee.setLastName(employee.getLastName());
        updatedEmployee.setStreet(employee.getStreet());
        updatedEmployee.setPostcode(employee.getPostcode());
        updatedEmployee.setCity(employee.getCity());
        updatedEmployee.setPhone(employee.getPhone());
        return repository.save(updatedEmployee);
    }

    public void delete(long id) {
        EmployeeEntity toDelete = getEmployeeById(id);
        repository.deleteById(id);
    }
}
