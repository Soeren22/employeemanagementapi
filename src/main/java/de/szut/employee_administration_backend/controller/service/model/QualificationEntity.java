package de.szut.employee_administration_backend.controller.service.model;

import lombok.*;
import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "Qualification")
public class QualificationEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = true)
    private String designation;

    @ManyToMany(mappedBy = "skills", cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    private Set<EmployeeEntity> employees = new HashSet<>();

    public void addEmployee(EmployeeEntity employeeEntity) {
        this.employees.add(employeeEntity);
    }

}
