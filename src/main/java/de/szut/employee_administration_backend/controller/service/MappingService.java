package de.szut.employee_administration_backend.controller.service;

import de.szut.employee_administration_backend.controller.dtos.*;
import de.szut.employee_administration_backend.controller.service.model.EmployeeEntity;
import de.szut.employee_administration_backend.controller.service.model.QualificationEntity;
import org.springframework.beans.factory.annotation.Autowired;
import de.szut.employee_administration_backend.controller.exceptionHandling.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import java.util.HashSet;
import java.util.Set;

@Service
public class MappingService {

    @Autowired
    private QualificationService qualificationService;

    public QualificationDTO mapQualificationToQualificationDTO(QualificationEntity qualification){
        QualificationDTO dto = new QualificationDTO();
        dto.setDesignation(qualification.getDesignation());
        return dto;
    }

    public QualificationEntity mapQualificationDTOToQualification(QualificationDTO dto){
        QualificationEntity qualification = new QualificationEntity();
        qualification.setDesignation(dto.getDesignation());
        return qualification;
    }

    public EmployeeNameAndSkillDataDTO mapEmployeeEntityToEmployeeNameAndSkillDTO(EmployeeEntity entity){
        EmployeeNameAndSkillDataDTO dto = new EmployeeNameAndSkillDataDTO();
        dto.setId(entity.getId());
        dto.setSurname(entity.getLastName());
        dto.setFirstname(entity.getFirstName());
        dto.setSkillSet(mapEntitySetToDTOSet(entity.getSkills()));
        return dto;
    }

    public EmployeeEntity mapEmployeeRequestDTOToEmployeeEntity(EmployeeRequestDTO dto){
        EmployeeEntity entity = new EmployeeEntity();
        entity.setLastName(dto.getLastName());
        entity.setFirstName(dto.getFirstName());
        entity.setStreet(dto.getStreet());
        entity.setPostcode(dto.getPostcode());
        entity.setCity(dto.getCity());
        entity.setPhone(dto.getPhone());
        return entity;
    }

    private Set<QualificationDTO> mapEntitySetToDTOSet(Set<QualificationEntity> entitySet){
        Set<QualificationDTO> result = new HashSet<>();
        for (QualificationEntity q: entitySet) {
            result.add(this.mapQualificationToQualificationDTO(q));
        }
        return result;
    }

    public EmployeeResponseDTO mapEmployeeEntityToEmployeeResponseDTO(EmployeeEntity entity){
        EmployeeResponseDTO dto = new EmployeeResponseDTO();
        dto.setId(entity.getId());
        dto.setLastName(entity.getLastName());
        dto.setFirstName(entity.getFirstName());
        dto.setStreet(entity.getStreet());
        dto.setPostcode(entity.getPostcode());
        dto.setCity(entity.getCity());
        dto.setPhone(entity.getPhone());
        return dto;
    }

    public EmployeesForAQualificationDTO mapToEmployeesForAQualificationDTO(String designation, Set<EmployeeEntity> employees){
        EmployeesForAQualificationDTO dto = new EmployeesForAQualificationDTO();
        dto.setDesignation(designation);
        for (EmployeeEntity e: employees) {
            EmployeeNameDataDTO employeeNameDataDTO = new EmployeeNameDataDTO();
            employeeNameDataDTO.setId(e.getId());
            employeeNameDataDTO.setSurname(e.getLastName());
            employeeNameDataDTO.setFirstname(e.getFirstName());
            dto.addEmployee(employeeNameDataDTO);
        }
        return dto;
    }
}
