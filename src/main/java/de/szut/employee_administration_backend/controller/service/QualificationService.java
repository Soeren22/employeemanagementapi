package de.szut.employee_administration_backend.controller.service;

import de.szut.employee_administration_backend.controller.exceptionHandling.ResourceNotFoundException;
import de.szut.employee_administration_backend.controller.service.model.EmployeeEntity;
import de.szut.employee_administration_backend.controller.service.model.QualificationEntity;
import de.szut.employee_administration_backend.controller.service.repository.QualificationRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class QualificationService {
    private final QualificationRepository repository;

    public QualificationService(QualificationRepository repository) {
        this.repository = repository;
    }

    public QualificationEntity create(QualificationEntity qualification) {
        return this.repository.save(qualification);
    }

    public List<QualificationEntity> readAll() {
        return this.repository.findAll();
    }

    public QualificationEntity readById(long id) {
        Optional<QualificationEntity> optionalQualification = this.repository.findById(id);
        if (optionalQualification.isEmpty()) {
            return null;
        }
        return optionalQualification.get();
    }

    public boolean qualificationExistsByDesignation(String designation){
         return this.repository.existsByDesignation(designation);
    }

    public QualificationEntity readByDesignation(String designation) {
        return this.repository.findByDesignation(designation);
    }

    public Set<EmployeeEntity> getAllEmployeesForAQualification(String designation) {
        QualificationEntity qualification = this.readByDesignation(designation);
        return qualification.getEmployees();
    }

   public void delete(String designation) {
        QualificationEntity qualification= this.repository.findByDesignation(designation);
        if(qualification != null){
            this.repository.delete(qualification);
        }
    }
}
