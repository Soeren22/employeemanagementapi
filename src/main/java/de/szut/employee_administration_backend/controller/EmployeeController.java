package de.szut.employee_administration_backend.controller;
import de.szut.employee_administration_backend.controller.dtos.*;
import de.szut.employee_administration_backend.controller.service.MappingService;
import de.szut.employee_administration_backend.controller.service.QualificationService;
import de.szut.employee_administration_backend.controller.service.model.EmployeeEntity;
import de.szut.employee_administration_backend.controller.service.model.QualificationEntity;
import de.szut.employee_administration_backend.controller.exceptionHandling.ResourceNotFoundException;
import de.szut.employee_administration_backend.controller.service.EmployeeService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.LinkedList;
import java.util.List;

@RestController
@RequestMapping("employee_api/employees")
public class EmployeeController {
    private final EmployeeService service;
    private final QualificationService qualiService;
    private final MappingService mappingService;

    private final String exceptionMessage = "EmployeeEntity not found on id = ";

    public EmployeeController(EmployeeService service, QualificationService qualiService, MappingService mappingService) {
        this.service = service;
        this.qualiService = qualiService;
        this.mappingService = mappingService;
    }

    @PostMapping()
    public ResponseEntity<EmployeeResponseDTO> createEmployee(@Valid @RequestBody EmployeeRequestDTO employeeDTO) {
        EmployeeEntity employee = this.mappingService.mapEmployeeRequestDTOToEmployeeEntity(employeeDTO);
        employee = this.service.create(employee);
        EmployeeResponseDTO responseDTO = this.mappingService.mapEmployeeEntityToEmployeeResponseDTO(employee);
        return new ResponseEntity<>(responseDTO, HttpStatus.CREATED);
    }

    @GetMapping()
    public ResponseEntity<List<EmployeeResponseDTO>> findAll() {
        List<EmployeeEntity> employeeList = this.service.getAllEmployees();
        if (employeeList.isEmpty()) {
            throw new ResourceNotFoundException("No employees found.");
        }
        List<EmployeeResponseDTO> response = new LinkedList<>();
        for (EmployeeEntity e: employeeList) {
            response.add(this.mappingService.mapEmployeeEntityToEmployeeResponseDTO(e));
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<EmployeeResponseDTO> findById(@PathVariable long id) {
        EmployeeEntity employee = service.getEmployeeById(id);
        if (employee == null) {
            throw new ResourceNotFoundException(exceptionMessage + id);
        }
        EmployeeResponseDTO dto = this.mappingService.mapEmployeeEntityToEmployeeResponseDTO(employee);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<EmployeeResponseDTO> updateEmployee(@PathVariable Long id, @Valid @RequestBody EmployeeRequestDTO updatedEmployeeDTO) {
        if(!this.service.employeeExistsById(id)){
            throw new ResourceNotFoundException("Employee not found on id = !"+ id);
        }
        EmployeeEntity updatedEmployee = this.mappingService.mapEmployeeRequestDTOToEmployeeEntity(updatedEmployeeDTO);
        updatedEmployee.setId(id);
        updatedEmployee = this.service.updateContactData(updatedEmployee);
        EmployeeResponseDTO dto = this.mappingService.mapEmployeeEntityToEmployeeResponseDTO(updatedEmployee);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteCustomer(@PathVariable Long id) {
        if(!this.service.employeeExistsById(id)){
            throw new ResourceNotFoundException(this.exceptionMessage + id);
        }
        this.service.delete(id);
    }

    @PostMapping("/{id}/qualifications")
    public ResponseEntity<EmployeeNameAndSkillDataDTO> addQualificationToEmployeeById(@PathVariable Long id,
                                                                              @Valid @RequestBody QualificationDTO qualificationDTO) {
        if(!this.service.employeeExistsById(id)){
            throw new ResourceNotFoundException("Employee not found on id = "+ id);
        }
        if(!this.qualiService.qualificationExistsByDesignation(qualificationDTO.getDesignation())){
            throw new ResourceNotFoundException("Qualification not found on designation = "+qualificationDTO.getDesignation());
        }
        QualificationEntity qualification = this.mappingService.mapQualificationDTOToQualification(qualificationDTO);
        EmployeeEntity employee = this.service.addQualification(id, qualification);
        EmployeeNameAndSkillDataDTO response = this.mappingService.mapEmployeeEntityToEmployeeNameAndSkillDTO(employee);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/{id}/qualifications")
    public ResponseEntity<EmployeeNameAndSkillDataDTO> findAllQualificationOfAEmployeeById(@PathVariable Long id) {
        if(!this.service.employeeExistsById(id)){
            throw new ResourceNotFoundException("EmployeeEntity not found on id = "+ id);
        }
        EmployeeEntity employee = this.service.getEmployeeById(id);
        EmployeeNameAndSkillDataDTO response = this.mappingService.mapEmployeeEntityToEmployeeNameAndSkillDTO(employee);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping("/{id}/qualifications")
    public ResponseEntity<EmployeeNameAndSkillDataDTO> removeQualificationFromEmployee(@PathVariable Long id, @Valid @RequestBody QualificationDTO qualificationDTO ) {
        if(!this.service.employeeExistsById(id)){
            throw new ResourceNotFoundException("EmployeeEntity not found on id = "+ id);
        }
        if(!this.qualiService.qualificationExistsByDesignation(qualificationDTO.getDesignation())){
            throw new ResourceNotFoundException("QualificationEntity not found on designation = "+qualificationDTO.getDesignation());
        }
        QualificationEntity qualification = this.mappingService.mapQualificationDTOToQualification(qualificationDTO);
        EmployeeEntity employee = this.service.removeQualification(id, qualification);
        EmployeeNameAndSkillDataDTO response = this.mappingService.mapEmployeeEntityToEmployeeNameAndSkillDTO(employee);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
