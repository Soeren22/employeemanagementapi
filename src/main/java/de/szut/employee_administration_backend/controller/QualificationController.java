package de.szut.employee_administration_backend.controller;

import de.szut.employee_administration_backend.controller.dtos.EmployeeRequestDTO;
import de.szut.employee_administration_backend.controller.dtos.EmployeeResponseDTO;
import de.szut.employee_administration_backend.controller.dtos.EmployeesForAQualificationDTO;
import de.szut.employee_administration_backend.controller.dtos.QualificationDTO;
import de.szut.employee_administration_backend.controller.exceptionHandling.ResourceNotFoundException;
import de.szut.employee_administration_backend.controller.service.model.EmployeeEntity;
import de.szut.employee_administration_backend.controller.service.model.QualificationEntity;
import de.szut.employee_administration_backend.controller.service.QualificationService;
import de.szut.employee_administration_backend.controller.service.MappingService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(value = "employee_api/qualifications")
public class QualificationController {
    private final QualificationService service;
    private final MappingService mappingService;

    public QualificationController(QualificationService service, MappingService mappingService) {
        this.service = service;
        this.mappingService = mappingService;
    }

    @PostMapping
    public ResponseEntity<QualificationDTO> createQualification(@RequestBody @Valid QualificationDTO dto) {
        QualificationEntity qualification = this.mappingService.mapQualificationDTOToQualification(dto);
        qualification = this.service.create(qualification);
        dto = this.mappingService.mapQualificationToQualificationDTO(qualification);
        return new ResponseEntity<>(dto, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<List<QualificationDTO>> findAll() {
        List<QualificationEntity> qualificationList = this.service.readAll();
        if (qualificationList.isEmpty()) {
            throw new ResourceNotFoundException("No qualifications found!");
        }
        List<QualificationDTO> response = new LinkedList<>();
        for (QualificationEntity q : qualificationList) {
            QualificationDTO dto = this.mappingService.mapQualificationToQualificationDTO(q);
            response.add(dto);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteQualificationByDesignation(@RequestBody QualificationDTO dto) {
        if (!this.service.qualificationExistsByDesignation(dto.getDesignation())) {
            throw new ResourceNotFoundException("QualificationEntity not found on designation = " + dto.getDesignation());
        }
        this.service.delete(dto.getDesignation());
    }

    @GetMapping("/designation/employees")
    public ResponseEntity<EmployeesForAQualificationDTO> findAllEmployeesByQualification(@RequestBody QualificationDTO dto) {
        if (this.service.qualificationExistsByDesignation(dto.getDesignation())) {
            Set<EmployeeEntity> employees = this.service.getAllEmployeesForAQualification(dto.getDesignation());
            EmployeesForAQualificationDTO response = this.mappingService.mapToEmployeesForAQualificationDTO(dto.getDesignation(), employees);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } else {
            throw new ResourceNotFoundException("Qualification not found on designation = " + dto.getDesignation());
        }
    }
}
