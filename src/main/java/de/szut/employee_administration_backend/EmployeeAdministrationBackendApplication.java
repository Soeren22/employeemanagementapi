package de.szut.employee_administration_backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployeeAdministrationBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(EmployeeAdministrationBackendApplication.class, args);
    }

}
